//load express
var express = require('express');
//create instance of app
var app = express();

//configure port
var port = parseInt(process.argv[2]) || 3000

app.listen(port , function(rep,require){
    console.log('application started on %d' , port)
});