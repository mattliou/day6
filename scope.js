var laundryRoom = 'Basement';
var mailRoom = 'Room 1A';
function myApartment() {
  var mailBoxNumber = 'Box 3';
  laundryRoom = 'In-unit';
  console.log('Mail box: ' + mailBoxNumber + ', Laundry:' + laundryRoom);
};

console.log('Laundry: ' + laundryRoom + ' , Mail: ' + mailRoom);
//console.log(mailBoxNumber); <-- error will not work as hidden away inside myApartment() which there is no access to 
myApartment();