var hello = function(name) {
    console.log('hello %s' , name)
}

hello('world');

var name = 'barney'

var fred = {
    name: 'fred',
    email: 'fred@gmail.com',
    hello: function() {
        console.log('hello %s' , this.name); // this. function refers to holder of the object - in this case, var fred
    }
};

var barney = {
    name: 'Barney',
}

fred.hello()

var myhello = fred.hello;
myhello();

barney.helloBarney = fred.hello
barney.helloBarney();
 
console.log(fred.email)
console.log(fred["email"])
var xyz="email"
console.log(xyz) //print value of var xyz defined in line 19
console.log(fred[xyz]) // same as:  console.log(fred["email"])
console.log(fred.xyz) //both 22&23 will give error as xyz is not a key within object var fred
console.log(fred["xyz"])

console.log(fred.name) // will work because name is a valid key in fred object

// Officially, using the square brackets is the primary way to access keys in an object
// However, typing object["keyname"] is a bit annoying, so there is a 2nd convenient way:
// object.keyname 
//
// Square brackets is more powerful because you can use variables to decide the name 
// of the key. For example:  object[employee_id]  (where employee_id is a variable).
// In this case you can't use the dot notation: object.employee_id , because it
// interprets employee_id as a string, not the contents of a variable.







var a = 'name'
console.log()